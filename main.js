import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false
import uView from 'uview-ui';
Vue.use(uView);
App.mpType = 'app'
import fabric from 'fabric'
import * as THREE from "three";
import { DoubleSide, BackSide, FrontSide } from 'three';
import TGALoader from 'static/js/loaders/TGALoader.js';
import {setPath,load,MTLLoader}  from 'static/js/loaders/MTLLoader.js';
import  OBJLoader  from 'static/js/loaders/OBJLoader.js';
import DDSLoader from 'static/js/loaders/DDSLoader.js';
import  TrackballControls from  'static/js/loaders/TrackballControls.js';

Vue.use(fabric);
Vue.use(DoubleSide)
Vue.use(THREE);
// Vue.use(TrackballControls);
// Vue.component('uni-pop', uniPop) 
const app = new Vue({
    ...App
})
app.$mount()
